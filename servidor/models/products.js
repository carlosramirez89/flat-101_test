const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
    title:String,
    price: String,
    url: String
});

module.exports = mongoose.model('product', ProductSchema);