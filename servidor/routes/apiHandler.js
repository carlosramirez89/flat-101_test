const router = require('express').Router();
const apiProducts = require('./api/products');

router.use('/productos', apiProducts);


module.exports = router;