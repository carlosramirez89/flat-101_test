var express = require('express');
var router = express.Router();
const Product = require("../../models/products");


router.get('/', async (req, res) => {
    try {
        const product = await Product.find();
        res.json(product);
    }

    catch(error) {
        res.status(400).json({error: "No se han podido recuperar la lista de productos"})
    }
});

router.post('/new-product', async (req, res) => {
    Product.create(req.body); 
    try {
        res.send('Producto creado con éxito');
    }

    catch(error) {
        res.status(400).json({error: "No se han podido recuperar la lista de productos"})
    }
});


module.exports = router;